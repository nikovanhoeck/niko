import componenten.clients.ChatClientImpl;
import componenten.clients.ChatClientSkeleton;
import componenten.communication.NetworkAddress;
import componenten.servers.ChatServer;
import componenten.servers.ChatServerStub;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ChatClientImplTest {
    String addres;
    int port;
    NetworkAddress serverAddress;
    ChatServer serverStub;
    ChatClientSkeleton client;


    @Before
    public void startClient() {
        addres = "192.168.0.183:";
        port = 8080;
        serverAddress = new NetworkAddress(addres,port);
        serverStub = new ChatServerStub(serverAddress);
        //client = new ChatClientSkeleton();
    }

    @Test
    public void run() {
        client.run();
    }
}