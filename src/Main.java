import componenten.clients.ChatClient;
import componenten.clients.ChatClientImpl;
import componenten.clients.ChatClientSkeleton;
import componenten.communication.NetworkAddress;
import componenten.presenters.ChatPresenter;
import componenten.servers.ChatServer;
import componenten.servers.ChatServerStub;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application{

    private static ChatServer server;

    public static void main(String[] args) {
        if (args.length != 2) {
            System.err.println("Usage: java Client <contactsIP> <contactsPort>");
            System.exit(1);
        }
        int port = Integer.parseInt(args[1]);
        NetworkAddress serverAddress = new NetworkAddress(args[0], port);
        server = new ChatServerStub(serverAddress);
        launch(args);

    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/componenten/views/chatview.fxml"));
        Parent root = fxmlLoader.load();
        ChatPresenter presenter = fxmlLoader.getController();

        primaryStage.setTitle("Chatscreen");
        primaryStage.setScene(new Scene(root, 600, 400));


        ChatClient client = new ChatClientImpl(server,"Niko");

        ChatClientSkeleton clientSkeleton = new ChatClientSkeleton();
        clientSkeleton.addListener(client);


        //TODO Dit moet als laatste gerund worden maar zorgt ervoor dat de rest niet meer aangeraakt wordt (ui bv)
        //TODO Threads maken???
        clientSkeleton.run();

        presenter.initialize(client);
        primaryStage.show();


    }
}
