package componenten.presenters;

public interface TextReceiver {
    void receive(String text);
}
