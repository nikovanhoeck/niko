package componenten.presenters;

import componenten.clients.ChatClient;
import componenten.clients.ChatClientImpl;
import componenten.clients.ChatClientSkeleton;
import componenten.servers.ChatServer;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class ChatPresenter implements TextReceiver{

    private ChatClient client;

    @FXML Pane view;
    @FXML Label name;
    @FXML TextArea messages;
    @FXML TextField messageToSend;
    @FXML Button sendBtn,exitBtn;

    @FXML
    public void initialize(ChatClient client) {
        this.client = client;
        client.setTextReceiver(this);
        this.name.setText(client.getName());
        Stage stage = (Stage) view.getScene().getWindow();
        stage.setTitle("Chat: " + name);
    }

    @FXML
    public void sendMsg() {
        String msg = messageToSend.getText();
        messageToSend.clear();
        client.send(msg);
    }

    @FXML
    public void exit() {
        this.client.unregister();
        System.exit(0);

    }

    @Override
    public void receive(String text) {
        messages.setText(messages.getText() + "\n\n: " +text);
    }

}
