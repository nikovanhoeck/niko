package componenten.servers;

import componenten.clients.ChatClient;
import componenten.communication.MessageManager;
import componenten.communication.MethodCallMessage;
import componenten.communication.NetworkAddress;

public class ChatServerStub implements ChatServer {
    private final NetworkAddress serverAdress;
    private final MessageManager messageManager;

    public ChatServerStub(NetworkAddress serverAdress) {
        this.serverAdress = serverAdress;
        this.messageManager = new MessageManager();
    }

    @Override
    public void register(ChatClient chatClient) {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(),"register");
        message.setParameter("name",chatClient.getName());
        messageManager.send(message, serverAdress);
        checkEmptyReply();
    }

    @Override
    public void unregister(ChatClient chatClient) {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(),"unregister");
        message.setParameter("name",chatClient.getName());
        messageManager.send(message, serverAdress);
        checkEmptyReply();
    }

    @Override
    public void send(String name, String msg) {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(),"send");
        message.setParameter("content",msg);
        message.setParameter("name",msg);
        messageManager.send(message, serverAdress);
        checkEmptyReply();
    }

    private void checkEmptyReply() {
        String value = "";
        while (!"Ok".equals(value)) {
            MethodCallMessage reply = messageManager.wReceive();
            if (!"result".equals(reply.getMethodName())) {
                continue;
            }
            value = reply.getParameter("result");
        }
    }
}
