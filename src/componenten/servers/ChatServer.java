package componenten.servers;

import componenten.clients.ChatClient;

public interface ChatServer {
    void register(ChatClient chatClient);
    void unregister(ChatClient chatClient);
    void send(String name, String msg);
}
