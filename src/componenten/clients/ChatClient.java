package componenten.clients;

import componenten.communication.NetworkAddress;
import componenten.presenters.TextReceiver;

public interface ChatClient {
    void send(String msg);
    void receive(String msg);
    String getName();
    void setTextReceiver(TextReceiver listener);
    void unregister();
    void register(NetworkAddress myAddress);
}
