package componenten.clients;

import componenten.communication.NetworkAddress;
import componenten.presenters.TextReceiver;
import componenten.servers.ChatServer;
import componenten.servers.ChatServerStub;

public class ChatClientImpl implements ChatClient {

    private ChatServer chatServer;
    private TextReceiver listener;
    private String name;

    public ChatClientImpl(ChatServer chatServer, String name) {
        this.chatServer = chatServer;
        this.name = name;
        //this.chatServer.register(this);
    }

    public String getName() {
        return name;
    }

    public void send(String message) {
        chatServer.send(name, message);
    }

    public void receive(String message) {
        if (listener == null) return;
        listener.receive(message);
    }

    public void setTextReceiver(TextReceiver listener) {
        this.listener = listener;
    }

    public void unregister() {
        chatServer.unregister(this);
    }

    @Override
    public void register(NetworkAddress myAddress) {
        chatServer.register(new ChatClientImpl(new ChatServerStub(myAddress),"Niko"));
    }
}
