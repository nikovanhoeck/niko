package componenten.clients;

import componenten.communication.MessageManager;
import componenten.communication.MethodCallMessage;

public class ChatClientSkeleton {
    private final MessageManager messageManager;
    private ChatClient listener;
    private boolean running = false;

    public ChatClientSkeleton() {
        this.messageManager =  new MessageManager();
        System.out.println("my address = "  + messageManager.getMyAddress());
        running = true;
    }

    private void sendEmptyReply(MethodCallMessage request) {
        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(),"result");
        reply.setParameter("result","Ok");
        messageManager.send(reply,request.getOriginator());
    }

    private void handleReceive(MethodCallMessage request) {
        String msg = request.getParameter("message");
        listener.receive(msg);
        sendEmptyReply(request);
    }

    private void handlegetName(MethodCallMessage request) {
        sendReplyGetName(request, listener.getName());

    }
    private void sendReplyGetName(MethodCallMessage request, String name) {
        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(),"result");
        reply.setParameter("result","Ok");
        reply.setParameter("name",name);
        messageManager.send(reply,request.getOriginator());
    }

    public void addListener(ChatClient listener) {
        this.listener = listener;
    }

    public void run() {
        Thread skeletonThread = new Thread() {
            public void run() {
                while (running)
                {
                    MethodCallMessage request = messageManager.wReceive();
                    switch (request.getMethodName()) {
                        case "receive":
                            handleReceive(request);
                            break;
                        case "getName":
                            handlegetName(request);
                            break;
                    }
                }
            }
        };
    }
}
